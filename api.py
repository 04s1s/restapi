from flask import Flask, request
from flask_restful import Resource, Api

#Here is where I set the value
ScriptSetVar = 'This was set on line 5 in the script!'

#Setup webserver with Flask
app = Flask(__name__)
api = Api(app)

#Define before resource setup
#Each path will have a class with the same setup
#class Name(Resource):

class Hello(Resource):
    def get(self):
    	print('Hi!')
    	return 'You called Hello and now console says Hi!'
class Var(Resource):
    def get(self):
        return 'You wanted to get a preset var so here it is! ' + ScriptSetVar

class Input(Resource):
    def get(self, ui):
    	#UI is the input that comes after /input/ (As seen on line 36, Request 3)
    	return 'You input ' + ui + '! so there it is!'
        
        




#Add a path to call each thing
#The setup is ([Name from above], [Path name. Etc '/example'])
api.add_resource(Hello, '/hello') # Request 1
api.add_resource(Var, '/var') # Request 2
#To have an input have the var in <>. In this case the var is ui so we have <ui>
api.add_resource(Input, '/input/<ui>') # Request 3

#Start the webserver on the set port
if __name__ == '__main__':
     app.run(port='5002')